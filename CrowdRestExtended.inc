<?php


/**
 * Extend the main crowd module REST class.
 */
class CrowdRestExtended extends crowd_rest_client {
  
  /**
   * Perform a generic search in Crowd using the Crowd Query Language.
   *
   * @param string $entity_type
   *   The entity type to search (must be either "user" or "group").
   * @param string $restriction
   *   A search restriction expressed in the Crowd Query Language.
   * @param string $start_index
   *   An optional offset value for the query.
   * @param string $max_results
   *   An optional limit for the query.
   * @return array $results
   *   A numerically keyed array of search results or boolean FALSE on failure.
   */
  public function search_cql($entity_type, $restriction, $start_index = 0, $max_results = 1000) {
    $this->request_method = 'GET';
    $final_url = $this->request_url . '/search?' . drupal_http_build_query(array('entity-type' => $entity_type, 'restriction' => $restriction, 'start-index' => $start_index, 'max-results' => $max_results));
    $options = array(
      'headers' => $this->request_headers,
      'method' => $this->request_method,
      'max_redirects' => $this->max_redirects,
      'timeout' => $this->timeout,
    );
    $result = drupal_http_request($final_url, $options);
    if ($result->code == '200' || $result->code =='201') {
      $data = json_decode($result->data);
      $results = array();
      foreach($data->{$entity_type . 's'} as $key => $value) {
        $results[] = $value->name;
      }
      return $results;
    }
    else {
      watchdog('crowd', 'REST: Crowd CQL search failed. Error: %data', array('%data' => serialize($result)), WATCHDOG_ERROR);
      return FALSE;
    }
  }
  
  /**
   * Get user info based on a username.
   *
   * @param string $username
   *   The username to fetch details for.
   * @return
   *   An array containing the user data points or FALSE on failure.
   */
  public function user_data_from_name($username) {
    $this->request_method = 'GET';
    $final_url = $this->request_url . '/user?' . drupal_http_build_query(array('username' => $username, 'expand' => 'attributes'));
    $options = array(
      'headers' => $this->request_headers,
      'method' => $this->request_method,
      'max_redirects' => $this->max_redirects,
      'timeout' => $this->timeout,
    );
    $result = drupal_http_request($final_url, $options);
    if ($result->code == '200' || $result->code =='201') {
      $data = json_decode($result->data);
      return $data;
    }
    else {
      watchdog('crowd', 'REST: Crowd user_get_from_name failed. Error: %data', array('%data' => serialize($result)), WATCHDOG_ERROR);
      return FALSE;
    }
  }
   
  /**
   * Add a new user to Crowd
   *
   * @param string $user
   *   The username to add.
   * @param array $user_data
   *   An associative array containing the user data (fields) to add, including:
   *   - 'first-name': First name.
   *   - 'last-name': Last name.
   *   - 'email': email address
   *   - 'password': user password
   *   - 'display-name' (optional): Display name.
   *   - 'active' (optional): Whether to set user as active (true/false)
   * @return boolean
   *   TRUE on successful addition and FALSE on failure.
   */
  public function add_user($username, $user_data) {
    $this->request_method = 'POST';
    // Calculate display name.
    if (empty($user_data['display-name'])) {
      $user_data['display-name'] = $user_data['first-name'] . ' ' . $user_data['last-name'];
    }
    // Calculate active status.
    $active = 'true';
    if (isset($user_data['active']) && (!$user_data['active'] || $user_data['active'] === 'false')) {
      $active = 'false';
    }
    $XML = new SimpleXMLElement("<?xml version='1.0' encoding='utf-8'?><user></user>");
    $XML->addAttribute('name', htmlspecialchars($username, ENT_NOQUOTES, 'UTF-8'));
    $XML->addAttribute('expand', 'attributes');
    $XML->addChild('first-name', htmlspecialchars($user_data['first-name'], ENT_NOQUOTES, 'UTF-8'));
    $XML->addChild('last-name', htmlspecialchars($user_data['last-name'], ENT_NOQUOTES, 'UTF-8'));
    $XML->addChild('display-name', htmlspecialchars($user_data['display-name'], ENT_NOQUOTES, 'UTF-8'));
    $XML->addChild('email', htmlspecialchars($user_data['email'], ENT_NOQUOTES, 'UTF-8'));
    $XML->addChild('active', $active);
    $XML->addChild('attributes', '');
    $pass = $XML->addChild('password');
    $pass->addChild('value', htmlspecialchars($user_data['password'], ENT_NOQUOTES, 'UTF-8'));
    $this->request_data = $XML->asXML();
    $final_url = $this->request_url . '/user';
    $options = array(
      'headers' => $this->request_headers,
      'method' => $this->request_method,
      'data' => $this->request_data,
      'max_redirects' => $this->max_redirects,
      'timeout' => $this->timeout,
    );
    $result = drupal_http_request($final_url, $options);
    if ($result->code == '200' || $result->code =='201') {
      return TRUE;
    } 
    else {
      watchdog('crowd', 'REST: Crowd user addition failed. Error: %data', array('%data' => serialize($result)), WATCHDOG_ERROR);
      return FALSE;
    }
  }
  
  /**
   * Update an existing user in Crowd.
   *
   * @param string $user
   *   The username to update.
   * @return array $user_data
   *   An associative array of user data (fields) to update, should include:
   *   - 'first-name': First name.
   *   - 'last-name': Last name.
   *   - 'email': email address.
   *   - 'display-name': Display name.
   *   - 'active': Whether to set user as active (true/false).
   * @return boolean
   *   TRUE on successful update and FALSE on failure.
   */
  public function update_user($username, $user_data) {
    $this->request_method = 'PUT';
    // Calculate display name.
    if (empty($user_data['display-name'])) {
      $user_data['display-name'] = $user_data['first-name'] . ' ' . $user_data['last-name'];
    }
    // Calculate active status.
    $active = 'true';
    if (isset($user_data['active']) && (!$user_data['active'] || $user_data['active'] === 'false')) {
      $active = 'false';
    }
    $XML = new SimpleXMLElement("<?xml version='1.0' encoding='utf-8'?><user></user>");
    $XML->addAttribute('name', htmlspecialchars($username, ENT_NOQUOTES, 'UTF-8'));
    $XML->addAttribute('expand', 'attributes');
    $XML->addChild('first-name', htmlspecialchars($user_data['first-name'], ENT_NOQUOTES, 'UTF-8'));
    $XML->addChild('last-name', htmlspecialchars($user_data['last-name'], ENT_NOQUOTES, 'UTF-8'));
    $XML->addChild('display-name', htmlspecialchars($user_data['display-name'], ENT_NOQUOTES, 'UTF-8'));
    $XML->addChild('email', htmlspecialchars($user_data['email'], ENT_NOQUOTES, 'UTF-8'));
    $XML->addChild('active', $active);
    $this->request_data = $XML->asXML();
    $final_url = $this->request_url . '/user?' . drupal_http_build_query(array('username' => $username));
    $options = array(
      'headers' => $this->request_headers,
      'method' => $this->request_method,
      'data' => $this->request_data,
      'max_redirects' => $this->max_redirects,
      'timeout' => $this->timeout,
    );
    $result = drupal_http_request($final_url, $options);
    if ($result->code == '200' || $result->code =='204') {
      return TRUE;
    } 
    else {
      watchdog('crowd', 'REST: Crowd user update failed. Error: %data', array('%data' => serialize($result)), WATCHDOG_ERROR);
      return FALSE;
    }
  }
  
  
  /**
   * Get the list of users in a Crowd group.
   *
   * @param string $group_name
   *   The group name to retrieve members from.
   * @param string $start_index
   *   An optional offset value for the query.
   * @param string $max_results
   *   An optional limit for the query.
   * @return array $results
   *   A numerically keyed array of group members or boolean FALSE on failure.
   */
  public function list_group_members($group_name, $start_index = 0, $max_results = 1000) {
    $this->request_method = 'GET';
    $final_url = $this->request_url . '/group/user/direct?' . drupal_http_build_query(array('groupname' => $group_name, 'start-index' => $start_index, 'max-results' => $max_results));
    $options = array(
      'headers' => $this->request_headers,
      'method' => $this->request_method,
      'max_redirects' => $this->max_redirects,
      'timeout' => $this->timeout,
    );
    $result = drupal_http_request($final_url, $options);
    if ($result->code == '200' || $result->code =='201') {
      $data = json_decode($result->data);
      $results = array();
      foreach($data->users as $key => $value) {
        $results[] = $value->name;
      }
      return $results;
    }
    else {
      watchdog('crowd', 'REST: Crowd get group members failed. Error: %data', array('%data' => serialize($result)), WATCHDOG_ERROR);
      return FALSE;
    }
  }
  
  /**
   * Add a Crowd user to a group.
   *
   * @param string $username
   *   The username to add to a group.
   * @param string $groupname
   *   The groupname to add the user to.
   * @return boolean
   *   TRUE on successful addition and FALSE on failure.
   */
  public function add_user_to_group($username, $groupname) {
    $this->request_method = 'POST';
    $XML = new SimpleXMLElement("<?xml version='1.0' encoding='utf-8'?><group></group>");
    $XML->addAttribute('name', htmlspecialchars($groupname, ENT_NOQUOTES, 'UTF-8'));
    $this->request_data = $XML->asXML();
    $final_url = $this->request_url . '/user/group/direct?' . drupal_http_build_query(array('username' => $username));
    $options = array(
      'headers' => $this->request_headers,
      'method' => $this->request_method,
      'data' => $this->request_data,
      'max_redirects' => $this->max_redirects,
      'timeout' => $this->timeout,
    );
    $result = drupal_http_request($final_url, $options);
    if ($result->code == '200' || $result->code =='201') {
      return TRUE;
    } 
    else {
      watchdog('crowd', 'REST: Could not add %user to %group in Crowd. Got error code %code', array('%user' => $username, '%group' => $groupname, '%code' => $result->code), WATCHDOG_ERROR);
      return FALSE;
    }
  }

  /**
   * Remove a Crowd user from a group.
   *
   * @param string $username
   *   The username to remove from a group.
   * @param string $groupname
   *   The groupname to remove the user from.
   * @return boolean
   *   TRUE on successful removal and FALSE on failure.
   */
  public function delete_user_from_group($username, $groupname) {
    $this->request_method = 'DELETE';
    $this->request_data = "";
    $final_url = $this->request_url . '/user/group/direct?' . drupal_http_build_query(array('username' => $username, 'groupname' => $groupname));
    $options = array(
      'headers' => $this->request_headers,
      'method' => $this->request_method,
      'data' => $this->request_data,
      'max_redirects' => $this->max_redirects,
      'timeout' => $this->timeout,
    );
    $result = drupal_http_request($final_url, $options);
    if ($result->code == '204') {
      return TRUE;
    } 
    else {
      watchdog('crowd', 'REST: Could not delete %user from %group in Crowd. Got error code %code', array('%user' => $username, '%group' => $groupname, '%code' => $result->code), WATCHDOG_ERROR);
      return FALSE;
    }
  }
  
}
